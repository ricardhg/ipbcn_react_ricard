import React from 'react';
import Personaje from './Personaje';

class Lista extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            usuarios: []
        };
    }

    componentDidMount(){
        let url = "https://randomuser.me/api/?results=100";
        fetch(url)
          .then(datos => datos.json())
          .then(datosConvertidos => 
                this.setState({usuarios: datosConvertidos.results})
          )
          .catch(err => console.log(err));
      }

    render(){

        if(this.state.usuarios.length===0){
            return <h2>Esperando datos...</h2>;
        }
        let i = 1;
        let nombres = this.state.usuarios.map( el => <Personaje  tetoca={el} /> );

  
        return (
            <>
                <h1>Lista</h1>
                {nombres}
        

            </>
        )
    }
}

export default Lista;
