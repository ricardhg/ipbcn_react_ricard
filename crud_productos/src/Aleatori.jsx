import React from "react";
//importem dos variables/constants amb la url de la api, les definim dins Utils una sola vegada per tota la aplicació
import { API_URL,IMG_URL } from "./Utils";

//importem component Link de navegació que fem servir en aquest component Lista
import { Link } from "react-router-dom";

//definim classe Lista
class Aleatori extends React.Component {
  
  //mètode constructor PRIMER en executar-se
  constructor(props) {
    super(props);
    //definim state només amb variable productos com a array buida
    this.state = {
      productos: []
    };
  }

  //mètode TERCER en executar-se
  componentDidMount() {
    //connectem a la API demanant els "productos"
    //la url completa on fem fetch és: "http://192.168.1.10:3000/api/productos"
    //el mètode per defecte és GET, no caldria indicar-lo, ho afegim per mostrar com es fa
    fetch(API_URL + "productos", {method: "GET"})
      //les dades (data) arriben sempre com un string en format json, les convertim en objecte javascrip amb data.json()
      .then(data => data.json())
      // ara aquest dadesConvertides ja té el resultat de data.json(), que és un array d'objectes
      // modifiquem el state assignant aquestes dadesConvertides a "this.state.productos"
      .then(dadesConvertides => this.setState({ productos: dadesConvertides }))
      //en cas d'error, el mostrem a la consola
      .catch(err => console.log(err));
  }

  //mètode render, SEGON en executar-se
  //es torna a executar cada vegada que es canvia el State amb this.setState()
  render() {
    //si no tenim encara cap producte a this.state.productos render retorna missatge "cargando datos"
    if (this.state.productos.length===0) {
      return <h4>Cargando datos...</h4>;
    }

    let numProductes = this.state.productos.length;
    let aleatori = Math.floor(Math.random()*numProductes)+1;
    let producteAleatori = this.state.productos[aleatori];

    return (
      <>
        <h1>Aleatori...</h1>
        
        <h3>{producteAleatori.nombre}</h3>
        <img src={producteAleatori.imatge} />

      </>
    );
  }
}

//exportem el component Lista
export default Aleatori;

