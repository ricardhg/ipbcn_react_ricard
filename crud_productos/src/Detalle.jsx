import React from "react";
import {API_URL, IMG_URL} from './Utils';

class Detalle extends React.Component {

  constructor(props){
    super(props);
    this.state={
      producto:null,
      id: this.props.match.params.idProducto
    };

  } 

  componentDidMount(){

    fetch(API_URL+"productos/"+this.state.id)
    .then(data => data.json())
    .then(data => this.setState({producto: data[0]}))
    .catch(err => console.log(err));

  }

  render() {

    if (this.state.producto===null){
      return <h4>Cargando datos...</h4>;
    }

    let producto = this.state.producto;

    return (
      <>
        <h1>Detalle...</h1>
        <hr/>
        <h3>{producto.nombre}</h3>
        <h3>{producto.precio}</h3>
        <h3>{producto.categoria}</h3>
        <img src={IMG_URL+producto.imagen} alt={producto.nombre}/>

      </>
    );
  }
}

export default Detalle;
