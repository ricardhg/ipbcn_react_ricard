import React from "react";
import Lista from './Lista';


class App extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      personaje: "nadie"
    }
  }
  componentDidMount(){
    let url = "https://randomuser.me/api";
    fetch(url)
      .then(datos => datos.json())
      .then(datosConvertidos => {
        let usuario0 = datosConvertidos.results[0];
        let nombre = usuario0.name.title +" "+ usuario0.name.first +" " + usuario0.name.last;
        console.log(nombre);
        this.setState({personaje: nombre, datosPersonaje: usuario0})
      })
      .catch(err => console.log(err));
  }

  render(){
    
    if (this.state.personaje==="nadie"){
      return <h4>Cargando datos...</h4>;
    }

    return(
      <>
      
        <h1>Hola {this.state.personaje}</h1>
        <img src={this.state.datosPersonaje.picture.large} alt="personaje" />

        <Lista />
      </>
    );
  }
}

export default App;
