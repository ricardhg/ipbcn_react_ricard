import React from "react";

import "bootstrap/dist/css/bootstrap.min.css";
import "./app.css";

import Navegacion from "./Navegacion";
import Lateral from "./Lateral";
import Foto from "./Foto";
import Producte from "./Producte";
import Peu from "./Peu";

import {productos} from "./Datos";

class App extends React.Component {

  constructor(props){
    super(props);

    this.state={
      productos: []
    }
 
  }

  componentDidMount(){

    let url = "http://192.168.1.10:3000/api/productos";
    fetch(url)
    .then(datosJson => datosJson.json())
    .then(datosConvertidos => this.setState({productos:datosConvertidos}))
    .catch(err => console.log(err));

  }


  render() {
    let i = 1;
    let tarjetas = this.state.productos.map(el => <Producte key={i++} nombre={el.nombre} foto={"http://192.168.1.10:8080/"+el.imagen} precio={el.precio+"€"} /> )
    return (<>
        <Navegacion />
        <div className="container">
          <div className="row">
            <div className="col-lg-3">
              <Lateral />
            </div>
            <div className="col-lg-9">
              <Foto />
              <div className="row">
                {tarjetas}
              </div>
            </div>
          </div>
        </div>
        <Peu />
      </>
    );
  }
}

export default App;
