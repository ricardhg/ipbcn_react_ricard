import React from 'react';

class Entrada extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            valor: this.props.valor,
        };
        this.cambiar = this.cambiar.bind(this);
    }

    cambiar(e){
        let valor = e.target.value
        this.setState({valor})
    }

    render(){
      

        return (
            <>
                <input value={this.state.valor} onChange={this.cambiar} />
                <h1>{this.state.valor}</h1>
            </>
        );
    }
}

export default Entrada;
