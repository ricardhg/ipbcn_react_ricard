/*
Component App: és el component base del projecte, el crida index.js
Hem creat un projecte genèric nano i després hem instal.lat els component addicionals de boostrap i navegació, així:

npx nano-react-app crud_productos
cd crud_productos
npm install
npm instrall bootstrap reactstrap react-router-dom --save

*/

//importem react
import React from "react";
//importem el css general de boostrap
import 'bootstrap/dist/css/bootstrap.min.css';
//carreguem els components necessaris per la navegació
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
//importem els components de reactstrap que fem servir dins App
import { Container, Row, Col } from 'reactstrap';

//importem tots els components de l'aplicació que fem servir en aquest App
import Inicio from './Inicio.jsx';
import Lista from './Lista.jsx';
import Nuevo from './Nuevo.jsx';
import Elimina from './Elimina.jsx';
import Edita from './Edita.jsx';
import Detalle from './Detalle.jsx';
import Aleatori from './Aleatori.jsx';
import P404 from './P404.jsx';

//component App, el definim com a classe derivada de React.Component
class App extends React.Component {

  /**
   * mètode render
   * mostra menú d'opcions de navegació i defineix TOTES les rutes de l'aplicació, vinculades a cada component
   * el que queda fora de "switch" es veurà en totes les "pàgines"
   * en canvi dels components que hi ha dins el switch només en veurem un, el que coincideixi amb la ruta del navegador
   * si la ruta del navegador es incorrecta o no existeix, es mostrarà l'últim switch
   */

  render() {
    return (
      <>
        <BrowserRouter>
          <Container>
            <Row>
              <Col>
                <ul>
                  <li> <Link to="/">Inicio</Link> </li>
                  <li> <Link to="/Lista">Lista</Link> </li>
                  <li> <Link to="/Nuevo">Nuevo</Link> </li>
                  <li> <Link to="/Aleatori">Aleatori</Link> </li>
                </ul>
              </Col>
            </Row>
            <Switch>
              <Route exact path="/" component={Inicio} />
              <Route path="/Lista" component={Lista} />
              <Route path="/Aleatori" component={Aleatori} />
              <Route path="/Nuevo" component={Nuevo} />
              <Route path="/Detalle/:idProducto" component={Detalle} />
              <Route path="/Edita/:idProducto" component={Edita} />
              <Route path="/Elimina/:idProducto" component={Elimina} />
              <Route component={P404} />
            </Switch>
          </Container>
        </BrowserRouter>
      </>
    )
  }
}

//exportem classe App com a default, això fa que des de index es pugui importar amb: import App from './App'
export default App;
