-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: rude
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.8-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `animalentidad`
--

DROP TABLE IF EXISTS `rude_animalentidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rude_animalentidad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identidad` int(11) DEFAULT NULL,
  `idanimal` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `animalentidad`
--

LOCK TABLES `rude_animalentidad` WRITE;
/*!40000 ALTER TABLE `animalentidad` DISABLE KEYS */;
INSERT INTO `rude_animalentidad` VALUES (1,1,1),(2,2,1),(3,2,2),(4,3,1),(5,3,2),(6,3,3),(7,3,4),(8,3,5),(9,3,6),(10,3,7),(11,3,8),(12,3,9),(13,4,2),(14,5,2),(15,6,2),(16,7,1),(17,7,2),(18,7,3),(19,7,4),(20,7,5),(21,7,6),(22,7,7),(23,7,8),(24,7,9),(25,8,1),(26,8,2),(27,8,3),(28,8,4),(29,8,5),(30,8,6),(31,8,7),(32,8,8),(33,8,9),(34,9,1),(35,9,2),(36,11,1),(37,11,2),(38,11,3),(39,11,4),(40,11,5),(41,11,6),(42,11,7),(43,11,8),(44,11,9),(45,12,1),(46,12,2),(47,12,3),(48,12,4),(49,12,5),(50,12,6),(51,12,7),(52,12,8),(53,12,9),(54,13,1),(55,13,2),(56,13,3),(57,13,4),(58,13,5),(59,13,6),(60,13,7),(61,13,8),(62,13,9),(63,14,1),(64,14,2),(65,15,1),(66,15,2),(67,16,1),(68,16,2),(69,17,1),(70,17,2),(71,17,3),(72,17,4),(73,17,5),(74,17,6),(75,17,7),(76,17,8),(77,17,9),(78,18,1),(79,18,2),(80,19,1),(81,19,2),(82,20,1),(83,20,2),(84,21,1),(85,21,2),(86,21,3),(87,22,3),(88,22,4),(89,22,5),(90,22,6),(91,22,7),(92,22,8),(93,22,9),(94,23,3),(95,23,4),(96,23,5),(97,23,6),(98,23,7),(99,23,8),(100,23,9),(101,24,3),(102,24,4),(103,24,5),(104,24,6),(105,24,7),(106,24,8),(107,24,9),(108,25,3),(109,25,4),(110,25,5),(111,25,6),(112,25,7),(113,25,8),(114,25,9),(115,26,3),(116,26,4),(117,26,5),(118,26,6),(119,26,7),(120,26,8),(121,26,9),(122,27,3),(123,27,4),(124,27,5),(125,27,6),(126,27,7),(127,27,8),(128,27,9),(129,28,6),(130,28,7),(131,28,8),(132,28,9),(133,29,6),(134,29,8),(135,29,9),(136,30,7),(137,30,8);
/*!40000 ALTER TABLE `animalentidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `animales`
--

DROP TABLE IF EXISTS `rude_animales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rude_animales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `especie` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `animales`
--

LOCK TABLES `rude_animales` WRITE;
/*!40000 ALTER TABLE `animales` DISABLE KEYS */;
INSERT INTO `rude_animales` VALUES (1,'perros'),(2,'gatos'),(3,'hurones'),(4,'conejos'),(5,'roedores'),(6,'aves'),(7,'anfibios'),(8,'reptiles'),(9,'otros');
/*!40000 ALTER TABLE `animales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comentario`
--

DROP TABLE IF EXISTS `rude_comentario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rude_comentario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `comentario` text DEFAULT NULL,
  `puntuacion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comentario`
--

LOCK TABLES `rude_comentario` WRITE;
/*!40000 ALTER TABLE `comentario` DISABLE KEYS */;
/*!40000 ALTER TABLE `comentario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entidades`
--

DROP TABLE IF EXISTS `rude_entidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rude_entidades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `web` varchar(100) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `lunes` varchar(45) DEFAULT NULL,
  `martes` varchar(45) DEFAULT NULL,
  `miercoles` varchar(45) DEFAULT NULL,
  `jueves` varchar(45) DEFAULT NULL,
  `viernes` varchar(45) DEFAULT NULL,
  `sabado` varchar(45) DEFAULT NULL,
  `domingo` varchar(45) DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `servicios` varchar(45) DEFAULT NULL,
  `observaciones` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entidades`
--

LOCK TABLES `rude_entidades` WRITE;
/*!40000 ALTER TABLE `entidades` DISABLE KEYS */;
INSERT INTO `rude_entidades` VALUES (1,'Fundació Altarriba','Ronda de Sant Pere 60, 08010','https://www.altarriba.org/','934 120 073','altarribla@altarriba.org','10:00 - 20:00','10:00 - 20:00','10:00 - 20:00','10:00 - 20:00','10:00 - 20:00','10:00 - 14:00',NULL,'Protectora',NULL,NULL),(2,'Lliga per a la protecció d\'animals i plantes de Barcelona','Carrer del Guarda Anton 10, 08035','https://www.protectorabcn.es/','934 170 124','info@protectorabcn.es','10:00 - 17:30','10:00 - 17:30','10:00 - 17:30','10:00 - 17:30','10:00 - 17:30','10:00 - 13:00','10:00 - 17:30','Protectora',NULL,NULL),(3,'Asociación Defensa Derechos Animal','Carrer de Bailèn 164, 08037','https://www.addaong.org/es/','934 591 601','adda@addaong.org','9:30 - 14:00','9:30 - 14:00','9:30 - 14:00','9:30 - 14:00','9:30 - 14:00',NULL,NULL,'Protectora',NULL,NULL),(4,'Gatuari','Carrer de Sant Lluís 14, 08012','http://gatuari.cat/','931 99 23 39',NULL,'','10:30 - 14:00, 17:00 - 21:00','10:30 - 14:00, 17:00 - 21:00','10:30 - 14:00, 17:00 - 21:00','10:30 - 14:00, 17:00 - 21:00','10:30 - 14:00, 17:00 - 21:00','10:30 - 14:00, 17:00 - 21:00','Protectora',NULL,NULL),(5,'El Jardinet dels Gats','Carrer Nou de la Rambla 14, 08001','http://www.eljardinetdelsgats.org/es/','636 38 21 94',NULL,NULL,NULL,NULL,NULL,NULL,'12:00 - 14:00',NULL,'Protectora',NULL,NULL),(6,'Espai de Gats','Carrer de Terol 29, 08012','https://www.espaidegats.com/',NULL,NULL,'17:00 - 21:00','17:00 - 21:00','17:00 - 21:00','10:30 - 13:30, 17:00 - 21:00','10:30 - 13:30, 17:00 - 22:00','10:30 - 13:30, 16:00 - 22:00','10:30 - 13:30, 17:00 - 21:00','Protectora',NULL,NULL),(8,'Fundación para el Asesoramiento y Acción en Defensa de los Animales','Rambla Prim 155-157 Entresuelo 1ª, 08020','http://faada.org/','93 624 55 38',NULL,'9:00 - 18:00','9:00 - 18:00','9:00 - 18:00','9:00 - 18:00','9:00 - 18:00',NULL,NULL,'Asociación',NULL,NULL),(9,'Libera! Asociación Animalista','Carrer del Montseny 49, pral., 1ª, 08012','http://liberaong.org/','931 42 62 67 ','libera@liberaong.org',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Asociación',NULL,NULL),(10,'Fundació Silvestre Protecció animal','Carrer de Mejía Lequerica 28, 08028','http://fundaciosilvestre.org/','93 853 82 89','info@fundaciosilvestre.org','10.00 - 14.00, 15.00 - 18.30','10.00 - 14.00, 15.00 - 18.30','10.00 - 14.00, 15.00 - 18.30','10.00 - 14.00, 15.00 - 18.30','9:00 - 14:30',NULL,NULL,'Asociación',NULL,NULL),(11,'Hospital Veterinario Glories','Avenida Diagonal 237, 08013','https://www.hospitalveterinariglories.com/','93 246 08 05, 93 763 89 96','info@hospitalveterinariglories.com','10:00 - 21:00','10:00 - 21:00','10:00 - 21:00','10:00 - 21:00','10:00 - 21:00','10:00 - 14:00',NULL,'Hospital veterinario','Urgencias 24h',''),(12,'Hospital Veterinari Provença','Calle Provença 214','http://www.veterinaribarcelona.com/','937 070 249, 610 423 626','info@hospitalvetpro.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Hospital veterinario',NULL,'Abierto 24h'),(13,'Hospital Veterinario del Mar','Carrer Marina 69','https://veterinariadelmar.com/','930 217 812','','09:30 - 20:30','09:30 - 20:30','09:30 - 20:30','09:30 - 20:30','09:30 - 20:30','10:00 - 13:00',NULL,'Hospital veterinario','Urgencias 24h','Sala de hospitalización exclusiva para gatos '),(14,'Balmes Vet Clinica Veterinaria','Carrer Balmes 205, Local 1, 08006','http://www.balmesvet.com/','931 173 173','atencioclient@balmesvet.com','10:00 - 20:00','10:00 - 20:00','10:00 - 20:00','10:00 - 20:00','10:00 - 20:00','10:00 - 14:00',NULL,'Hospital veterinario','Urgencias 24h','De 1/2 h a 1 h de Parking gratuíto en la call'),(15,'Hospital Veterinari de Catalunya','Avenida Sarrià 137-A, 08017',' https://hvc.cat/es/clinicas-hvc/sarria/','932 057 238','sarria@hvc.cat','09:00 - 21:00','09:00 - 21:00','09:00 - 21:00','09:00 - 21:00','09:00 - 21:00','09:00 - 21:00','09:00 - 21:00','Hospital veterinario',NULL,NULL),(16,'Oncovet barcelona','Avenida Diagonal 237','https://www.oncovetbcn.com/','932 460 805','info@oncovetbcn.com','08:30 - 17:00','08:30 - 17:00','08:30 - 17:00','08:30 - 17:00','08:30 - 17:00',NULL,NULL,'Hospital veterinario',NULL,'Requiere cita previa'),(17,'Hospital Veterinari Montjuic','Carrer Mèxic 30, 08004','https://hvmontjuic.com/','934 237 711 ','info@hvmontjuic.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Hospital veterinario','Urgencias 24h','Horas  concertadas, parking gratuito para cli'),(18,'Hospital Ars Veterinaria','Carrer dels Cavallers 37, 08034','http://arsveterinaria.es/es/',' 932 858 400','info@arsveterinaria.es','08:00 - 21:00','08:00 - 21:00','08:00 - 21:00','08:00 - 21:00','08:00 - 21:00','09:00 - 14:00',NULL,'Hospital veterinario','Urgencias 24h',NULL),(19,'AVIAM!1 Veterinari Solidari','Gran Via de les Corts Catalanes 963, 08018','https://www.aviam.cat/',' 936 344 041','info@aviam.cat','10:00 - 20:00','10:00 - 20:00','10:00 - 20:00','10:00 - 20:00','10:00 - 20:00','10:00 - 14:00',NULL,'Veterinario solidario','Urgencias',NULL),(20,'AVIAM!2 Veterinari Solidari','Ronda Sant Pere 60, 08010','https://www.aviam.cat/','934 120 073','info@aviam.cat','10:00 - 13:00, 16:30h - 20:00h','10:00 - 13:00, 16:30h - 20:00h','10:00 - 13:00, 16:30h - 20:00h','10:00 - 13:00, 16:30h - 20:00h','10:00 - 13:00, 16:30h - 20:00h','10:00 - 14:00',NULL,'Veterinario solidario','Urgencias',NULL),(21,'Clínica Veterinaria AlphaBcn','Travessera de les Corts 180, 08028','https://www.veterinarialescorts.es/es/','934  340 300, 656 867 978','cvabcn@gmail.com','10:00 - 14:00, 16:30h - 20:00h','10:00 - 14:00, 16:30h - 20:00h','10:00 - 14:00, 16:30h - 20:00h','10:00 - 14:00, 16:30h - 20:00h','10:00 - 14:00, 16:30h - 20:00h','10:00 - 14:00',NULL,'Veterinario solidario','Servicio de urgencias telefónicas 24 horas, s',NULL),(22,'Clínica Veterinaria Exotics - Veterinario Exoticos','Carrer de Balmes 423, 08022','http://www.clinicaveterinariaexotics.com/','934 178 922,  urgencias 639 353 133','info@clinicaveterinariaexotics.com','9:30 - 21:00','9:30 - 21:00','9:30 - 21:00','9:30 - 21:00','9:30 - 21:00','10:00 - 14:00',NULL,'Veterinario exotico','Urgencais 24h',NULL),(23,'Exovets Centro Veterinario de Animales Exóticos Barcelona','Carrer de Baliarda 34B, 08030 ','http://exovets.com/','933 113 613, 689 134 094 ','info@exovets.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Veterinario exotico','Consultas a domicilio',NULL),(24,'Exomed Veterinaria','Carrer Balmes 222, 08006','https://www.exomedveterinaria.com/','932 372 004, Consultas fuera de horario 696 2','info@exomedveterinaria.com','10:00 - 14:00, 17:00 - 20:00','10:00 - 14:00, 17:00 - 20:00','10:00 - 14:00, 17:00 - 20:00','10:00 - 14:00, 17:00 - 20:00','10:00 - 14:00, 17:00 - 20:00','10:00 - 13:30',NULL,'Veterinario exotico','Urgencias 24h',NULL),(25,'Maragall Exòtics','Ronda Guinardó 164, baixos, 08041','http://maragallexotics.com/','934 365 888','','10:30 - 13:30, 17:00 - 20:30','10:30 - 13:30, 17:00 - 20:30','10:30 - 13:30, 17:00 - 20:30','10:30 - 13:30, 17:00 - 20:30','10:30 - 13:30, 17:00 - 20:30','11:00 - 14:00',NULL,'Veterinario exotico','Urgencias 24h',NULL),(26,'Vetex, Centro Veterinario de Animales Exóticos','Carrer de Viladomat 147, 08015',NULL,'933 251 499','mail@vetex.es','10:00 - 21:00','10:00 - 21:00','10:00 - 21:00','10:00 - 21:00','10:00 - 21:00','10:00 - 14:00',NULL,'Veterinario exotico','Urgencias 24h',NULL),(27,'Els Altres - Clínica Veterinaria','Carrer Rosselló 274','http://curemelsaltres.com/','931 621 275','','10:00 - 13:30, 16:30 - 20:00','10:00 - 13:30, 16:30 - 20:00','10:00 - 13:30, 16:30 - 20:00','10:00 - 13:30, 16:30 - 20:00','10:00 - 13:30, 16:30 - 20:00','10:00 - 13:00',NULL,'Veterinario exotico','Urgencias 24h',NULL),(28,'Centre de Fauna de Torreferrusa','Ctra. Sabadell, 08130 Santa Perpètua de Mogoda','http://bit.ly/2rIhrvr','935 617 017, 935 600 052','crf.torreferrussa.dmah@gencat.cat',' 8:30 - 20:30 ',' 8:30 - 20:30 ',' 8:30 - 20:30 ',' 8:30 - 20:30 ',' 8:30 - 20:30 ',' 8:30 - 20:30 ',' 8:30 - 20:30 ','Centro de fauna',NULL,NULL),(29,'CRAM. Centre de Recuperació d’Animals Marins de Catalunya','Passeig de la Platja 28-30, 08820 El Prat de Llobregat','https://cram.org/','937 524 581, 937 524 581',NULL,'10:00 - 17:00','10:00 - 17:00','10:00 - 17:00','10:00 - 17:00','10:00 - 17:00','10:00 - 14:00','10:00 - 14:00','Centro de fauna',NULL,NULL),(30,'CRARC. Centre de Recuperació d’Amfibis i Rèptils de Catalunya','Santa Clara s/n 08783 Masquefa','http://www.crarc-comam.net/','937 726 396','crarc-masquefa@outlook.com',NULL,NULL,NULL,NULL,NULL,'10:00 - 13:00, 16:00 - 18:00','10:00 - 13:00','Centro de fauna',NULL,'Per a lliurament d\'animals cal trucar prèviam');
/*!40000 ALTER TABLE `entidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `especies`
--

DROP TABLE IF EXISTS `rude_especies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rude_especies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idanimal` int(11) DEFAULT NULL,
  `especie` varchar(45) DEFAULT NULL,
  `comentario` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `especies`
--

LOCK TABLES `rude_especies` WRITE;
/*!40000 ALTER TABLE `especies` DISABLE KEYS */;
INSERT INTO `rude_especies` VALUES (1,1,'Perro',NULL),(2,2,'Gato',NULL),(3,4,'Conejo',NULL),(4,3,'Hurón',NULL),(5,5,'Cobaya',NULL),(6,5,'Ratón',NULL),(7,5,'Hamster',NULL),(8,9,'Erizo',NULL),(9,6,'Pato',NULL),(10,6,'Cotorra',NULL),(11,6,'Gorrión',NULL),(12,6,'Paloma',NULL),(13,6,'Urraca',NULL),(14,6,'Estornino',NULL),(15,6,'Tórtola',NULL),(16,6,'Gaviota',NULL),(17,6,'Vencejo',NULL),(18,6,'Golondrina',NULL),(19,6,'Verdecillo',NULL),(20,6,'Lavandera blanca',NULL),(21,9,'Jabalí',NULL),(22,6,'Periquito',NULL),(23,6,'Agapornis',NULL),(24,6,'Carolina',NULL),(25,6,'Cacatúa',NULL),(26,6,'Amazona',NULL),(27,6,'Loro yaco',NULL),(28,8,'Tortuga',NULL),(29,6,'Canario',NULL),(30,6,'Jilguero',NULL),(31,8,'Camaleón',NULL),(32,8,'Iguana',NULL),(33,8,'Gecko',NULL),(34,8,'Tortuga de agua',NULL),(35,8,'Tortuga de tierra',NULL),(36,8,'Víbora',NULL),(37,8,'Boa',NULL),(38,8,'Pitón',NULL),(39,8,'Culebra',NULL),(40,5,'Chinchilla',NULL),(41,5,'Rata',NULL),(42,6,'Guacamayo',NULL),(43,7,'Salamandra',NULL),(44,7,'Tritón',NULL),(45,7,'Sapo',NULL),(46,7,'Rana',NULL),(47,9,'Cerdo vietnamita',NULL),(48,9,'Perrito de la praderas',NULL),(49,9,'Petauros',NULL),(50,6,'Ave rapaz',NULL),(51,8,'Lagarto',NULL),(52,5,'Ardilla',NULL),(53,6,'Gallina',NULL);
/*!40000 ALTER TABLE `especies` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-15 15:13:56
