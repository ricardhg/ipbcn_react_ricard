import React from "react";
import Titulo from "./Titulo";
import Gato from "./Gato";
import Boton from "./Boton";
import Entrada from "./Entrada";


export default function App() {
  return (
    <>
      <Titulo texto="Hola gato" color="blue" tipo="h1" />
      <Boton on="si" />
      <Boton on="no" />
      <Boton on="si" />
      <Boton on="no" />
  
      {/* <Entrada /> */}
      <br />
      <br />
      <Gato />

    </>
  )
};


