import React from "react";
import { Switch, Route } from "react-router-dom";
import Contenido1 from "./Contenido1";
import Contenido0 from "./Contenido0";
import Contenido2 from "./Contenido2";
import Contenido3 from "./Contenido3";
class Contenido extends React.Component {
  render() {
    return (
      <div className="contenido">
        <Switch>
          <Route exact path="/" component={Contenido0} />
          <Route path="/contenido1" component={Contenido1} />
          <Route path="/contenido2" component={Contenido2} />
          <Route path="/contenido3" component={Contenido3} />
        </Switch>
      </div>
    );
  }
}

export default Contenido;
