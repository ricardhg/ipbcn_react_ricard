import React from 'react';
import {NavLink} from 'react-router-dom';

class Lateral extends React.Component {
    render(){
        return (
            <div className="lateral">
                <ul className="nav nav-pills flex-column">
                    <li className="nav-item"><NavLink className="nav-link" to="/contenido1">Contenido 1</NavLink> </li>
                    <li className="nav-item"><NavLink className="nav-link" to="/contenido2">Contenido 1</NavLink> </li>
                    <li className="nav-item"><NavLink className="nav-link" to="/contenido3">Contenido 1</NavLink> </li>
                </ul>
            </div>
        );
    }
}

export default Lateral;


