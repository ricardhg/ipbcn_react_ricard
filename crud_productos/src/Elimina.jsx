import React from "react";
import { API_URL, IMG_URL } from "./Utils";

import { Redirect } from "react-router-dom";

class Elimina extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      producto: null,
      id: this.props.match.params.idProducto,
      volver: false
    };
    this.confirmaElimina = this.confirmaElimina.bind(this);
    this.cancelaElimina = this.cancelaElimina.bind(this);
  }

  componentDidMount() {
    fetch(API_URL + "productos/" + this.state.id)
      .then(data => data.json())
      .then(data => this.setState({ producto: data[0] }))
      .catch(err => console.log(err));
  }

  confirmaElimina() {
    fetch(API_URL + "productos/" + this.state.id, { method: "DELETE" })
      .then(data => data.json())
      .then(data => console.log(data))
      .then(() => this.setState({ volver: true }))
      .catch(err => console.log(err));
  }

  cancelaElimina() {
    this.setState({ volver: true });
  }

  render() {
    if (this.state.volver) {
      return <Redirect to="/lista" />;
    }
    if (!this.state.producto) {
      return <h4>Cargando datos...</h4>;
    }
    let producto = this.state.producto;
    return (
      <>
        <h1>Seguro que quieres eliminar: {producto.nombre} ?</h1>
        <hr />
        <button onClick={this.confirmaElimina}>SI</button>
        <button onClick={this.cancelaElimina}>NO</button>
      </>
    );
  }
}

export default Elimina;
