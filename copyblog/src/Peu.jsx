import React from "react";

class Peu extends React.Component {
  render() {
    return (
      <>
        <footer className="py-5 bg-dark">
          <div className="container">
            <p className="m-0 text-center text-white">Copyright © Catalogo</p>
          </div>
        </footer>
      </>
    );
  }
}

export default Peu;
