import React from "react";
import "./personaje.css";


class Personaje extends React.Component {
    render() {
        return (<>
        <div className="personaje">
            <div>
                <img src={this.props.tetoca.picture.large} />
            </div>
            <div>
                <h3>{this.props.tetoca.name.title} {this.props.tetoca.name.first}</h3>
                <h2>{this.props.tetoca.location.city}</h2>
                <p>{this.props.tetoca.email}</p>
            </div>

        </div>
    </>);
  }
}

export default Personaje;
