import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Container, Row, Col } from "reactstrap";
import "./app.css";
import Cabecera from "./Cabecera.jsx";
import Lateral from "./Lateral.jsx";
import Contenido from "./Contenido.jsx";
import {BrowserRouter} from 'react-router-dom';

export default () => (
  <BrowserRouter>
    <Container>
      <Row>
        <Col>
          <Cabecera />
        </Col>
      </Row>
      <Row>
        <Col sm="5" lg="3">
          <Lateral />
        </Col>
        <div className="col-sm-7 col-lg-9">
          <Contenido />
        </div>
      </Row>
    </Container>
  </BrowserRouter>
);
