import React from "react";
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import { Container, Row, Col } from 'reactstrap';
import "./estilos.css";

import Clientes from './Clientes';
import Pedidos from './Pedidos';


function Error() {
  return <h2>Página Error</h2>;
}

export default () => (
  <BrowserRouter>
    <div className="cabecera">

    <h1>Titulo general</h1>
    <ul>
      <li> <Link to="/">Clientes</Link> </li>
      <li> <Link to="/clientes/llastics">Clientes llastics</Link> </li>
      <li> <Link to="/manolo">Pedidos Manolo</Link> </li>
      <li> <Link to="/barcelona">Pedidos Barcelona</Link> </li>
    </ul>
    </div>
    <Switch>
      <Route exact path="/" component={Clientes} />
      <Route exact path="/clientes/:nombre" component={Clientes} />
      <Route path="/manolo" render={() => <Pedidos mensaje="El cliente es Bar Manolo" /> } />
      <Route path="/barcelona" render={() => <Pedidos mensaje="El cliente es Bar Barcelona" /> } />
      <Route component={Error} />
    </Switch>

  </BrowserRouter>
);
