import React from "react";
import {API_URL} from './Utils';
import { Redirect } from "react-router-dom";

class Edita extends React.Component {

  constructor(props){
    super(props);
    //guardamos en state los datos iniciales
    this.state={
     
      cargando: true,
      volver: false
    };

    //declaramos todos los métodos de la clase Edita que necesitan acceder a "this"
    this.handleInputChange=this.handleInputChange.bind(this);
    this.enviarForm=this.enviarForm.bind(this);
    this.volver=this.volver.bind(this);

  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value,
      cargando: false
    });
  }

  enviarForm(e) {
    e.preventDefault();
    console.log(this.state.nombre);
    let prod = {
      id: this.state.id,
      nombre: this.state.nombre,
      categoria: this.state.categoria,
      imagen: this.state.imagen,
      precio: this.state.precio*1.0
    }

    //aquí fem un fetch amb el mètode PUT que permet modificar un registre existent a la BDD
    fetch(API_URL+"productos", {
        method: 'PUT', 
        headers: new Headers({ 'Content-Type': 'application/json' }),
        body: JSON.stringify(prod)
      })
    .then(data => data.json())
    .then(data => console.log(data))
    .then(()=>this.setState({volver:true}))
    .catch(err => console.log(err));
    
  }

  //pedimos a la API los datos del producto que vamos a editar
  componentDidMount(){
    //leemos parámetro desde la dirección (url) del navegador
    //ej) para la url http://localhost:1234/edita/25
    //this.props.match.params.idProducto seria 25
    
    let id = this.props.match.params.idProducto;
    let urlParaPedirUnProducto = API_URL+"productos/"+id;

    fetch(urlParaPedirUnProducto, {method: "GET"})
    .then(data => data.json())
    // la API retorna un array de un solo producto, lo extraemos mediante data[0]
    // puesto que nos interesa librarnos del array
    // al hacer un setState(data[0]) todos los atributos del objeto data[0] se extraen y copian al state
    // de modo que tendremos this.state.nombre, this.state.precio, this.state.categoria, etc
    // si hiciésemos setState({producto:data[0]})
    // tendríamos que acceder a través de this.state.producto.nombre, this.state.producto.precio, this.state.producto.categoria, etc
    // i ara no se perquè he posat tot això en castellà, però fa mandra canviar-ho...
    .then(data => this.setState(data[0]))
    .catch(err => console.log(err));
  }


  volver(){
    this.setState({volver: true});
  }

  render() {

    if (this.state.volver===true){
      return <Redirect to="/lista" />;
    }

    if (this.state.cargando===true){
      return <h4>Cargando datos...</h4>;
    }



    return (
      <>
        <h1>Edita...</h1>
        <hr/>
        <form onSubmit={this.enviarForm} >
          <input type="text" onChange={this.handleInputChange} value={this.state.nombre} name="nombre" placeHolder="nombre" />
          <input type="text" onChange={this.handleInputChange} value={this.state.categoria} name="categoria" placeHolder="categoria" />
          <input type="text" onChange={this.handleInputChange} value={this.state.precio} name="precio" placeHolder="precio" />
          <input type="submit" value="Enviar" />
        </form>
        <br/>
      <button onClick={this.volver}>Volver sin cambios</button>
      </>
    );
  }
}

export default Edita;
