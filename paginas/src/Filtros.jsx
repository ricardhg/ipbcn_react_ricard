
import React from 'react';


 class Filtros extends React.Component {

    constructor(props){
        super(props);
        this.state={
            pagina: this.props.pagina,
            cantidad: this.props.cantidad,
            codis:null,
            letraInicial: "--"
        }

        this.handleInputChange = this.handleInputChange.bind(this);

    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
    
        this.setState({
          [name]: value
        }, this.cargaDatos);
      }
    
    componentDidMount(){

        this.cargaDatos();
        
    }

    cargaDatos(){

        let url="";
        if (this.state.letraInicial==="--"){
            url = `http://192.168.1.10:3000/api/provincia?_size=100`;

        }else{

            url = `http://192.168.1.10:3000/api/provincia?_size=100&_where=(provincia,like,${this.state.letraInicial}~)`;
        }
    
        fetch(url)
        .then(data => data.json())
        .then(codis => this.setState({codis:codis}))
        .catch(err => console.log(err))
    }

  
    
    render(){
        if(this.state.codis===null){
            return <h1>Cargando datos</h1>
        }

        let lis = this.state.codis.map( el => <li key={el.provinciaid}>{el.provincia}</li>)

        let letras="abcdefghijklmnopqrstuvwxyz";
        let aletras = letras.split("");

        let opciones = aletras.map(el => <option key={el} value={el}>Letra: {el}</option>)
     
        //http://192.168.1.10:3000/api/provincia?_where=(provincia,like,a~)
        return (
            <>
    <h1>Provincias: "{this.state.letraInicial}"</h1>


                <br />
                <select name="letraInicial" onChange={this.handleInputChange}>
                   <option value="--">--</option>
                    {opciones}
                </select>
                <br />
                <br />
                <ul>
                    {lis}
                </ul>
             </>
        )


    }


}


export default Filtros;