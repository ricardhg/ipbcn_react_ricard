-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: table_tea
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `empresas`
--

DROP TABLE IF EXISTS `tea_empresas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tea_empresas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tea` int(11) DEFAULT NULL,
  `empresa` text,
  `ubicacion` text,
  `contato` text,
  `contrasena` varchar(45) DEFAULT NULL,
  `comentarios` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresas`
--

LOCK TABLES `tea_empresas` WRITE;
/*!40000 ALTER TABLE `empresas` DISABLE KEYS */;
/*!40000 ALTER TABLE `empresas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tea`
--

DROP TABLE IF EXISTS `tea_tea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tea_tea` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_ES` text,
  `tipo_CA` text,
  `tipo_EN` varchar(45) DEFAULT NULL,
  `pictograma` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=171 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tea`
--

LOCK TABLES `tea_tea` WRITE;
/*!40000 ALTER TABLE `tea` DISABLE KEYS */;
INSERT INTO `tea` VALUES (1,'Agencia de Modelos','Agència de Models','Models Agency','agencia_de_modelos.png'),(2,'Agencia de Viajes','Agència de Viatges','Travel Agency','agencias_de_viajes.png'),(3,'Alferería','Alfereria','Pottery Shop','alfereria.png'),(4,'Alquiller de Coches','Lloguer de Cotxes','Car Rental','tienda_de_alquiler_de_coches.png'),(5,'Ambulatorio','Ambulatori','Ambulatory','ambulatorio.png'),(6,'Apicultura','Apicultura','Bee Products','tienda_de_apicultura.png'),(7,'Arreglos de Ropa','Arranjaments de Roba','Clothing Fix','tienda_de_arreglos_de_ropa.png'),(8,'Asociación de Personas con Autismo','Associació de Persones amb Autisme','Association of people with autism','asociacion_de_personas_con_autimo.png'),(9,'Atención al público','Atenció al públic','Information','atencion_al_publico.png'),(10,'Autoescuela','Autoescola','Driving School','autoescuela.png'),(11,'Ayudas Técnicas','Ajudes Tècniques','Technical Support','tienda_de_ayudas_tecnicas.png'),(12,'Ayuntamiento','Ajuntament','Town Hall','ayuntamiento.png'),(13,'Banco','Banc','Bank','banco.png'),(14,'Bar de tapas','Bar de tapes','Tapas','bar_de_tapas.png'),(15,'Bar','Bar','Pub','bar.png'),(16,'Bebidas','Begudes','Beverage Shop','tienda_de_bebidas.png'),(17,'Biblioteca','Biblioteca','Library','biblioteca(1).png'),(18,'Bicicletas','Bicicletes','Bike Shop','tienda_de_bicicletas.png'),(19,'Bolsos','Bosses de mà','Handbags','tienda_de_bolsos.png'),(20,'Bricolaje','Bricolatge','DIY Store','tienda_de_bricolaje.png'),(21,'Cafetería','Cafeteria','Cafe','cafeteria.png'),(22,'Cajero Automático','Caixer Automàtic','ATM','cajero_automatico.png'),(23,'Campanario','Campanari','Bell Tower','campanario.png'),(24,'Capitolio','Capitoli','Capitol','capitolio.png'),(25,'Caramelos','Caramels','Candy Shop','tienda_de_caramelos.png'),(26,'Carnicería','Carnisseria','Butcher','carniceria.png'),(27,'Carpintería','Fusteria','Carpentry','carpinteria.png'),(28,'Cartucho de tinta','Cartutx de tinta','Ink Cartridge Store','tienda_de_cartuchos_de_tinta.png'),(29,'Casa','Casa','House','casas.png'),(30,'Casino','Casino','Casino','casino.png'),(31,'Casquería','Triperia','Farmhouse','casquerina.png'),(32,'Castillo','Castell','Castle','castillo(1).png'),(33,'Catedral','Catedral','Cathedral','catedral.png'),(34,'Caza y Pesca','Caça i Pesca','Hunting and fishing','tienda_de_caza_y_pesca.png'),(35,'Cementerio','Cementeri','Cementery','cementerio.png'),(36,'Centro Comercial','Centre Comercial','Shopping Mall','centro_comercial.png'),(37,'Cervecería','Cerveseria','Brewery','cerveceria.png'),(38,'Charcutería','Xarcuteria','Delicatessen','charcuteria(1).png'),(39,'Chiringuito','Xiringuito','Chiringuito','chiringuito.png'),(40,'Chocolatería','Xocolateria','Chocolate Shop','chocolateria.png'),(41,'Churrería','Xurreria','Churreria','churreria.png'),(42,'Cine','Cinema','Cinema','cine.png'),(43,'Coches','Cotxes','Car Selling','tienda_de_coches.png'),(44,'Colchones','Matalassos','Mattresses Shop','tienda_de_colchones.png'),(45,'Colegio','Col·legi','School','colegio(1).png'),(46,'Comercio','Comerç','Store','comercio(1).png'),(47,'Comisaría','Comissaria','Police Station','comisaria.png'),(48,'Complementos','Complements','Accessories','tienda_de_complementos.png'),(49,'Concesionario','Concessionari','Concessionaire','concesionario.png'),(50,'Congelados','Congelats','Frozen Food','tienda_de_congelados.png'),(51,'Conservatorio','Conservatori','Conservatory','conservatorio.png'),(52,'Copistería','Copisteria','Copy Shop','copisteria(1).png'),(53,'Correos','Correos','Post Office','correos(1).png'),(54,'Crepería','Creperia','Creperia','creperia.png'),(55,'Cristalería','Cristalleria','Glassware','cristaleria.png'),(56,'Decoración','Decoració','Decoration','tienda_de_decoracion.png'),(57,'Dentista','Dentista','Dentist','dentista.png'),(58,'Desportes','Esports','Sports','tienda_de_desportes.png'),(59,'Disfraces','Disfresses','Costume Store','tienda_de_disfarces.png'),(60,'Edificio','Edifici','Building','edificio.png'),(61,'Electricidad','Electricitat','Electrical Store','tienda_de_electricidad.png'),(62,'Electrónica','Electrònica','Electronic equipment','tienda_de_electronica.png.png'),(63,'Escayola','Escaiola','Plaster Store','tienda_de_escayola.png'),(64,'Escuela de Idiomas','Escoa d´Idiomes','Languages School','escuela_oficial_de_idiomas.png'),(65,'Escuela de Música','Escola de música','Music School','escuela_de_música.png'),(66,'Escuela de arte','Escola d´Art','Art School','escuela_de_arte.png'),(67,'Escuela','Escola','School','escuela.png'),(68,'Esquís','Esquís','Ski Equipments','tienda_de_esquis.png'),(69,'Estación','Estació','Train station','estacion(1).png'),(70,'Estanco','Estanc','Tobacco Shop','estanco.png'),(71,'Estética','Estètica','Aesthetic shop','tienda_de_estetica.png'),(72,'Farmacia','Farmàcia','Pharmacy','farmacia.png'),(73,'Feria','Fira','Fair','feria.png'),(74,'Ferretería','Ferreteria','Train station','ferreteria(1).png'),(75,'Floristería','Floristeria','Flower Shop','floristeria(1).png'),(76,'Fontanería','Fontaneria','Plumbing','fontaneria(1).png'),(77,'Fotografía','Fotografia','Photography','tienda_de_fotografia.png'),(78,'Frutería','Fruiteria','Fruit Market','fruteria(1).png'),(79,'Funicular','Funicular','Cable Car','funicular.png'),(80,'Galletería','Galeteria','Cookies Shop','galleteria.png'),(81,'Gasolinera','Benzinera','Gas Station','gasolinera(1).png'),(82,'Gimnasio','Gimnàs','Gym','gimnasio.png'),(83,'Hamburguesería','Hamburgueseria','Burguers','hamburgueseria.png'),(84,'Heladería','Gelateria','Ice Cream','heladeria(1).png'),(85,'Herboristería','Herboristeria','Herbalist shop','herboristeria.png'),(86,'Hospital Infantil','Hospital Infantil','Children Hospital','hospital_infantil.png'),(87,'Hospital','Hospital','Hospital','hospital.png'),(88,'Hotel accesible','Hotel accessible','Accessible hotel','hotel_accesible.png'),(89,'Hotel','Hotel','Hotel','hotel.png'),(90,'Iglesia','Esglèsia','Church','iglesia.png'),(91,'Industria','Indústria','Industry','industria(1).png'),(92,'Informática','Informàtica','Informatics','tienda_de_informatica.png'),(93,'Inmobiliaria','Immobiliària','Real estate','inmobiliaria.png'),(94,'Instituto','Institut','Institute','instituto.png'),(95,'Invernadero','Hivernacle','Greenhouse','invernadero.png'),(96,'Joyería','Joieria','Jewelry','joyeria(1).png'),(97,'Juguetería','Botiga de Joguines','Toy Store','tienda_de_juguetes.png'),(98,'Kebab','Kebab','Kebab','kebab.png'),(99,'Lavandería','Bugaderia','Laundry','lavanderia.png'),(100,'Lencería','Llenceria','Lingerie','lenceria.png'),(101,'Librería','Llibreria','Book Shop','libreria(1).png.png'),(102,'Lotería','Loteria','Lottery','loterias_y_apuestas.png'),(103,'Manualidades','Manualitats','Craft Store','tienda_de_manualidades.png'),(104,'Material de Construcciónn','Material de Construcció','Construction Material','tienda_de_materiales_de_construccion.png'),(105,'Menaje','Parament de casa','Household Store','tienda_de_menaje.png'),(106,'Mercería','Merceria','Haberdashery','merceria.png'),(107,'Metro','Metro','Metro','metro.png'),(108,'Mezquita','Mesquita','Mosque','mezquita.png'),(109,'Moda','Moda','Fashion Store','tienda_de_moda.png'),(110,'Motos','Motos','Motorbike Shop','tienda_de_motos.png'),(111,'Muebles de Cocina','Mobles de Cuina','Kitchen Furniture','tienda_de_muebles_de_cocina.png'),(112,'Muebles','Mobles','Furniture Store','tienda_de_muebles.png'),(113,'Museo','Museu','Museum','museo.png'),(114,'Máquinas de Coser','Màquines de Cosir','Sweing Machine Store','tienda_de_maquinas_de_coser.png'),(115,'Música','Música','Music Store','tienda_de_musica.png'),(116,'Observatorio','Observatori','Observatory','observatorio.png'),(117,'Oficina de Empleo','Oficina de Treball','Employment Office','oficina_de_empleo.png'),(118,'Oficina de Turismo','Oficina de Turisme','Tourist Office','oficina_de_turismo.png'),(119,'Oficina','Oficina','Oficce','oficina.png'),(120,'Óptica','Òptica','Optics','optica.png'),(121,'Ortopedia','Ortopèdia','Orthopedics','ortopedia.png'),(122,'Panadería','Fleca','Bakery','panaderia.png'),(123,'Papelería','Papereria','Stationery','papelaria.png'),(124,'Parada de Taxis','Parada de Taxis','Taxi','parada_de_taxis.png'),(125,'Parque Acuático','Parc Aquàtic','Aquatic Park','parque_acuatico.png'),(126,'Parque de Bomberos','Parc de Bombers','Fire Station','parque_de_bomberos(1).png'),(127,'Pastelería','Pastisseria','Cake Shop','pasteleria(1).png'),(128,'Patinaje','Patinatge','Skating Shop','tienda_de_patinaje.png'),(129,'Peluquería Canina','Perruqueria Canina','Pet Shop','peluqueria_canina.png'),(130,'Peluquería','Perruqueria','Hairdressing','peluqueria.png'),(131,'Pensión','Pensió','Pension','pension.png'),(132,'Perfumería','Perfumeria','Perfumery','perfumeria.png'),(133,'Pescadería','Peixateria','Fish Shop','pescaderia.png'),(134,'Pinturas','Pintures','Panting Store','tienda_de_pinturas.png'),(135,'Piscina Cuberta','Piscina Coberta','Indoor swimming pool','piscina_cubierta.png'),(136,'Pizzería','Pizzeria','Pizza','pizzeria.png'),(137,'Policía Local','Policia Local','Local Police','policia_local.png'),(138,'Pollería','Pollastreria','Chicken Shop','polleria(1).png'),(139,'Pozo','Pou','Water Well','pozo.png'),(140,'Prisión','Presó','Prison','prisiones.png'),(141,'Productos Ecológicos','Productes Ecològics','Eco Friendly Products','tienda_de_productos_ecologicos.png'),(142,'Puerto','Port','Port','puerto.png'),(143,'Párquing','Pàrquing','Parking','parking.png'),(144,'Quesería','Formatgeria','Cheese Shop','queseria.png'),(145,'Quiosco','Quiosc','Kiosk','quiosco(1).png'),(146,'Regalos','Regalos','Gift Shop','tienda_de_regalos.png'),(147,'Residencia de Ancianos','Residència d´Avis','Nursing Home','residencia_de_ancianos(1).png'),(148,'Residencia para discapacitados','Residància per a discapacitats','Disabled Nursing Home','residencia_para_discapacitados.png'),(149,'Restaurante','Restaurant','Restaurant','restaurante.png'),(150,'Ropa Infantil','Roba Infantil','Children Clothes','tienda_de_ropa_infantil.png'),(151,'Rótulos','Rètuls','Label Shop','tienda_de_rotulos.png'),(152,'Sagrada Familia','Sagrada Família','Sagrada Familia','Sagrada_Familia.png'),(153,'Serigrafía','Serigrafia','Serigraphy Shop','tienda_de_serigrafia.png'),(154,'Sinagoga','Sinagoga','Synagogue','sinagoga.png'),(155,'Supermercat','Supermercat','Supermarket','supermercado.png'),(156,'Taller Mecánico','Taller Mecànic','Mechanical Workshop','taller_mecanico.png'),(157,'Teatro','Teatre','Theater','teatro(1).png'),(158,'Telefonía','Telefonia','Mobile Shop','tienda_de_telefonia.png'),(159,'Templo','Temple','Temple','templo.png'),(160,'Tienda de Cuadros','Botiga de Quadres','Art Painting Material','tienda_de_cuadros.png'),(161,'Tienda de Mascotas','Botiga de Mascotes','Pet shop','tienda_de_animales.png'),(162,'Tienda de ropa','Botiga de Roba','Clothes Shop','tienda_de_ropa.png'),(163,'Tienda de zapatos','Botiga de Sabates','Shoe shop','zapateria.png'),(164,'Tranvía','Tramvia','Tram','tranvia.png'),(165,'Tren Turístico','Tren Turístic','Touristic Train','tren_turistico.png'),(166,'Tribunal','Tribunal','Court','tribunal.png'),(167,'Universidad','Universitat','University','universidad.png'),(168,'Verdulería','Botiga de Verdures','Greengrocery','verduleria.png'),(169,'Vinatería','Vinateria','Wine Store','tienda_de_vinos.png'),(170,'Zoológico','Zoològic','Zoo','zoologico.png');
/*!40000 ALTER TABLE `tea` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-15 17:03:01
