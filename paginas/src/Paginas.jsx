
import React from 'react';


 class Paginas extends React.Component {

    constructor(props){
        super(props);
        this.state={
            pagina: this.props.pagina,
            cantidad: this.props.cantidad,
            codis:[],
            nombresProvincias:[]
        }

        this.siguiente = this.siguiente.bind(this);
        this.anterior = this.anterior.bind(this);
        this.cargaDatos = this.cargaDatos.bind(this);
        this.cargaSelect = this.cargaSelect.bind(this);

    }

    anterior(){
        let nuevaPagina = this.state.pagina-1;
     
        this.setState({pagina: nuevaPagina}, this.cargaDatos);
    }
    siguiente(){
        this.setState({pagina: this.state.pagina+1}, this.cargaDatos);
    }

    componentDidMount(){

        fetch("http://192.168.1.10:3000/api/provincia/count")
        .then(data => data.json())
        .then(resultado => this.setState(
            {max_pagina: Math.floor(resultado[0].no_of_rows/this.state.cantidad)}, this.cargaSelect))
        .catch(err => console.log(err))

        this.cargaDatos();
        
    }

    cargaDatos(){

       
        let url = `http://192.168.1.10:3000/api/provincia?_size=${this.state.cantidad}&_p=${this.state.pagina}`;
    
        fetch(url)
        .then(data => data.json())
        .then(codis => this.setState({codis:codis}))
        .catch(err => console.log(err))
    }

    cargaSelect(){

        for(let paginaActual = 0; paginaActual<=this.state.max_pagina; paginaActual++){
            let url = `http://192.168.1.10:3000/api/provincia?_size=${this.state.cantidad}&_p=${paginaActual}`;
    
            console.log("pagina ", paginaActual);
            fetch(url)
            .then(data => data.json())
            .then(provincies => {
                let nombres=provincies.map(el => el.provincia);
                this.setState({nombresProvincias: [...this.state.nombresProvincias, ...nombres]})
            })
            .catch(err => console.log(err))
            
        }

   
    }
    
    render(){
        if(this.state.codis.length===0){
            return <h1>Cargando datos</h1>
        }

        let lis = this.state.codis.map( el => <li key={el.provinciaid}>{el.provincia}</li>)

        let opciones = this.state.nombresProvincias.map(el => <option key={el}>{el}</option>);
        return (
            <>
                <h1>Codis postals</h1>

                <br />
                <select>
                    {opciones}
                </select>
                <br />
                <br />
                <ul>
                    {lis}
                </ul>
                <button disabled={this.state.pagina===0} className="btn btn-primary" onClick={this.anterior}>Prev</button>
                <button disabled={this.state.pagina===this.state.max_pagina} className="btn btn-primary" onClick={this.siguiente}>Next</button>
            </>
        )


    }


}


export default Paginas;