import React from 'react';
import "./boton.css";
class Boton extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            activo: this.props.on,
        };
        this.clicar = this.clicar.bind(this);
    }

    clicar(){
        if(this.state.activo=="si"){
            this.setState({activo: "no"});
        } else {
            this.setState({activo: "si"});
        }
    }

    render(){
        let claseboton = "boton";
        if (this.state.activo=="si"){
            claseboton = "boton activo"; 
        }

        return (
            <>
            <div onClick={this.clicar} className={claseboton}></div>
            </>
        );
    }
}

export default Boton;
