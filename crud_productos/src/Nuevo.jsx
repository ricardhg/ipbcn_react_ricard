import React from "react";
import {API_URL, IMG_URL} from './Utils';
import { Redirect } from "react-router-dom";

class Nuevo extends React.Component {

  constructor(props){
    super(props);
    this.state={
      nombre: "",
      precio: "", 
      categoria: "",
      volver: false,
      
    };

    this.handleInputChange=this.handleInputChange.bind(this);
    this.creaProducto=this.creaProducto.bind(this);
    this.volver=this.volver.bind(this);

  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }

  creaProducto(e) {
    e.preventDefault();
    
    let prod = {
      nombre: this.state.nombre,
      categoria: this.state.categoria,
      precio: this.state.precio*1
    }

    fetch(API_URL+"productos", {
        method: 'POST', 
        headers: new Headers({ 'Content-Type': 'application/json' }),
        body: JSON.stringify(prod)
      })
    .then(data => data.json())
    .then(data => console.log(data))
    .then(()=>this.setState({volver:true}))
    .catch(err => console.log(err));
    
  }

  
  volver(){
    this.setState({volver: true});
  }


  render() {

    if (this.state.volver===true){
      return <Redirect to="/lista" />;
    }

    let luz = 12;

    return (
      <>
        <h1>Edita...{luz}</h1>
        <hr/>
        <form onSubmit={this.creaProducto} >
          <input type="text" onChange={this.handleInputChange} value={this.state.nombre} name="nombre" placeHolder="nombre" />
          <input type="text" onChange={this.handleInputChange} value={this.state.categoria} name="categoria" placeHolder="categoria" />
          <input type="text" onChange={this.handleInputChange} value={this.state.precio} name="precio" placeHolder="precio" />
          <input type="submit" value="Enviar" />
        </form>
        <br/>
      <button onClick={this.volver}>Volver sin cambios</button>
      </>
    );
  }
}

export default Nuevo;
