import React from "react";
import {categorias} from "./Datos";

class Lateral extends React.Component {
  
    render() {
        //["tecnología","ropa","accesorios", "oficina", "coche"];

        let cats = categorias.map(el => <a key={el} href="#" className="list-group-item">{el}</a>)

    return (
      <>
        <h1 className="my-4">La meva botiga</h1>
        <div className="list-group">
          {cats}
        </div>
      </>
    );
  }
}

export default Lateral;
