import React from 'react';
import fotoSuperior from "./original/900x350.png";

class Foto extends React.Component {
    render(){
        return(
            <>
                <img className="img-fluid mb-4" src={fotoSuperior} alt="banner" />  
            </>
        );
    }
}

export default Foto;