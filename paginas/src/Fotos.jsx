
import React from 'react';


 class Fotos extends React.Component {

    constructor(props){
        super(props);
        this.state={
            teas:null,
            id:0
        }

        this.handleInputChange = this.handleInputChange.bind(this);

    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
    
        this.setState({
          [name]: value
        });
      }
    
    componentDidMount(){

        this.cargaDatos();
        
    }

    cargaDatos(){
  
       let url = `http://192.168.1.10:3000/api/tea_tea`;

        fetch(url)
        .then(data => data.json())
        .then(teas => this.setState({teas:teas, id: teas[0].id}))
        .catch(err => console.log(err))
    }

  
    
    render(){
        if(this.state.teas===null){
            return <h1>Cargando datos</h1>
        }

        let options = this.state.teas.map(el =>
            <option key={el.id} value={el.id}>{el.tipo_ES}</option>);

            let actual = this.state.teas.filter(el => el.id==this.state.id)[0];

        return(
            <>
            <h1>Teas</h1>
            <select onChange={this.handleInputChange} name="id">
                {options}
            </select>
            <br />
            <img src={"http://192.168.1.10:8080/tea/"+actual.pictograma} width="100px"/>
            <p>{actual.tipo_EN}</p>
            <p>{actual.tipo_CA}</p>
            </>

        )
        }

}


export default Fotos;