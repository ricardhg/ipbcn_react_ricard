
export let categorias = ["tecnología","ropa","accesorios", "oficina", "coche"];

export let productos = [
    {id:11, nombre: "zapato niño", precio: 30, imagen: "http://placekitten.com/700/400"} ,
    {id:22, nombre: "ordenador", precio: 1220, imagen: "http://placekitten.com/700/450"} ,
    {id:13, nombre: "taza", precio: 6.50, imagen: "http://placekitten.com/600/350"} ,
    {id:4, nombre: "ratón", precio: 14.00, imagen: "http://placekitten.com/750/500"} ,
    {id:5, nombre: "teclado", precio: 10.20, imagen: "http://placekitten.com/800/600"} ,
    {id:6, nombre: "pantalla", precio: 250, imagen: "http://placekitten.com/720/450"} ,
    {id:7, nombre: "llibreta", precio: 2.50, imagen: "http://placekitten.com/710/480"} ,
];