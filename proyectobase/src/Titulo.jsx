import React from "react";

// export default function Titulo(props) {
function Titulo(props) {
  let estilos = { color: props.color };
  if (props.tipo == "h1") {
    return <h1 style={estilos}>{props.texto}</h1>;
  } else {
    return <h2 style={estilos}>{props.texto}</h2>;
  }
}

export default Titulo;
